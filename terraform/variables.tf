variable ssh_user{
  default = "ansible"
}
variable project{
  default = "test-ilot"
}
variable cred_file{
  default = "./key.json"
}
variable ssh_key_pub{
  default = "./ssh_key.pub"
}
