


provider "google" {
  project = "${var.project}"
    credentials = file("${var.cred_file}")
}

resource "google_compute_instance" "vm-backend" {
  name         = "vm-backend"
  machine_type = "e2-micro"
  zone         = "us-east1-b"
  tags         = ["http","https"]

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-minimal-2204-lts"
    }
  }

  network_interface {
    network = "default"
  }
  metadata = {
    ssh-keys = "${var.ssh_user}:${file("${var.ssh_key_pub}")}"
   }
}

resource "google_compute_instance" "vm-frontend" {
  name         = "vm-frontend"
  machine_type = "e2-micro"
  zone         = "us-east1-b"
  tags         = ["http","https"]

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-minimal-2204-lts"
    }
  }

  network_interface {
    network = "default"
    access_config {
      # Include this section to give the VM an external IP address
    }
  }
   metadata = {
    ssh-keys = "${var.ssh_user}:${file("${var.ssh_key_pub}")}"
   }
}

resource "google_compute_firewall" "webserver_proxy" {
  name    = "ssh"
  network = "default"

  allow {
    protocol = "tcp"
    ports    = ["3128"]
  }
  
  source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_firewall" "webserver_ssh" {
  name    = "proxy"
  network = "default"

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }
  
  source_ranges = ["0.0.0.0/0"]
}

